package uz.pdp.myquery;

import jakarta.persistence.*;
import uz.pdp.entity.Student;

import java.util.List;

public class QueryJPQL2 {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ketmonUnit");
        EntityManager em = emf.createEntityManager();

        EntityTransaction transaction = em.getTransaction();
        transaction.begin();

//        Query query = em.createQuery("DELETE FROM Student s WHERE s.id = :id");
//        query.setParameter("id", 3);
//        query.executeUpdate();
//        TypedQuery<Student> typedQuery = em.createQuery("SELECT s FROM Student s WHERE s.id=:id", Student.class);
        TypedQuery<Student> typedQuery = em.createNamedQuery("ketmonQidir", Student.class);
        typedQuery.setParameter("id", 2);
        List<Student> resultList = typedQuery.getResultList();
        System.out.println(resultList);

        transaction.commit();


    }
}
