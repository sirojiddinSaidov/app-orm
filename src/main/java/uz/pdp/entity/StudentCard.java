package uz.pdp.entity;

import jakarta.persistence.*;
import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.*;

import java.time.LocalDate;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Builder.Default
    @Column(nullable = false)
    private LocalDate issuedAt = LocalDate.now();

    @Column(nullable = false)
    private LocalDate expiredAt;

    @Column(nullable = false, unique = true)
    private String code;

//    @OneToOne(mappedBy = "card")
//    private Student student;
}
