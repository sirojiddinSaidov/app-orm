package uz.pdp.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NamedQueries(value = {
        @NamedQuery(name = "ketmonQidir", query = "SELECT s FROM Student s WHERE s.id=:id")
})
public class Student {
    @Id
//    @SequenceGenerator(name = "stud_seq", allocationSize = 1)
//    @GeneratedValue(generator = "stud_seq")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String firstName;

//    @Column(nullable = false)
//    private String lastName;
//
//    @Column(nullable = false)
//    private LocalDate birthDate;

//    @Builder.Default
//    @ElementCollection
//    private Map<Integer, Address> addresses = new HashMap<>();

//    @Column(nullable = false, unique = true)
//    private String phoneNumber;

//    @OneToOne//one student to one student card
//    private StudentCard card;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Address> addresses;

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                '}';
    }
}
