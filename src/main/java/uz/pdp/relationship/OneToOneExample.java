package uz.pdp.relationship;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import uz.pdp.entity.Address;
import uz.pdp.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class OneToOneExample {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ketmonUnit");
        EntityManager em = emf.createEntityManager();

        EntityTransaction transaction = em.getTransaction();
        transaction.begin();

        Student student = Student.builder()
                .firstName("Ketmon")
                .build();

        List<Address> addresses = new ArrayList<>() {
            {
                addAll(List.of(Address.builder().region("Samarqand")
                                .district("Urgut")
                                .build(),
                        Address.builder().region("Toshkent")
                                .district("Olmazor")
                                .build()));
            }
        };

        student.setAddresses(addresses);

        em.persist(student);


        transaction.commit();


    }
}
