package uz.pdp.relationship;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import uz.pdp.entity.Address;
import uz.pdp.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class CascadeExample {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ketmonUnit");
        EntityManager em = emf.createEntityManager();

        EntityTransaction transaction = em.getTransaction();
        transaction.begin();

        Student student = em.find(Student.class, 1);

        em.remove(student);

        transaction.commit();


    }
}
