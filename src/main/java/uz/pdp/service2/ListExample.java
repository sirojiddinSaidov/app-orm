//package uz.pdp.service2;
//
//import jakarta.persistence.EntityManager;
//import jakarta.persistence.EntityManagerFactory;
//import jakarta.persistence.EntityTransaction;
//import jakarta.persistence.Persistence;
//import uz.pdp.entity.Address;
//import uz.pdp.entity.Student;
//
//import java.time.LocalDate;
//import java.util.*;
//
//public class ListExample {
//    public static void main(String[] args) {
//
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ketmonUnit");
//        EntityManager em = emf.createEntityManager();
//
//        EntityTransaction transaction = em.getTransaction();
//        transaction.begin();
//
//        Address a1 = Address.builder()
//                .region("Toshkent")
//                .district("Olmazor")
//                .build();
////        Address a2 = Address.builder()
////                .region("Samarqanda")
////                .district("Urgut")
////                .build();
//
//        Student student = Student.builder()
//                .firstName("ads")
//                .lastName("dasdas")
//                .birthDate(LocalDate.now())
//                .phoneNumber("78787878")
//                .build();
//
//        em.persist(student);
//
////        student.getAddresses()
////                .put(student.getId(), a1);
////        em.merge(student);
//
//
//        transaction.commit();
//    }
//}
