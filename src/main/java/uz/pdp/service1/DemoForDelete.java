package uz.pdp.service1;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import uz.pdp.entity.Student;

public class DemoForDelete {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ketmonUnit");
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        Student student = em.find(Student.class, 2);

        if (student != null)
            em.remove(student);

        em.getTransaction().commit();

    }
}