//package uz.pdp.service1;
//
//import jakarta.persistence.*;
//import uz.pdp.entity.Group;
//import uz.pdp.entity.Student;
//
//import java.time.LocalDate;
//
//public class DemoForInsert {
//    public static void main(String[] args) {
//        EntityManagerFactory entityManagerFactory =
//                Persistence.createEntityManagerFactory("ketmonUnit");
//
//        EntityManager entityManager = entityManagerFactory.createEntityManager();
//
//        entityManager.getTransaction().begin();
//
//        Student student = Student.builder()
//                .firstName("Ketmon")
//                .lastName("Uroqov")
//                .birthDate(LocalDate.of(2002, 11, 19))
//                .phoneNumber("+998991234561")
//                .build();
//
//        Student student2 = Student.builder()
//                .firstName("Tesha")
//                .lastName("Boltayev")
//                .birthDate(LocalDate.of(2000, 11, 19))
//                .phoneNumber("+998991234562")
//                .build();
//
//
//        entityManager.persist(Group.builder()
//                .name("G24")
//                .build());
//        entityManager.persist(Group.builder()
//                .name("G25")
//                .build());
//        entityManager.persist(student);
//        entityManager.persist(student2);
//
//        entityManager.getTransaction().commit();
//
//    }
//}